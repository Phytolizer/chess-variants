cmake_minimum_required(VERSION 3.15)

set(CMAKE_EXPORT_COMPILE_COMMANDS YES)

project(chessvariants)

set(CMAKE_CXX_STANDARD 17)

file(GLOB_RECURSE SOURCES "src/*.cc")
string(REPLACE ";" "\n" SOURCES_STR "${SOURCES}")
message("Sources:")
message("${SOURCES_STR}")

add_executable(chessvariants ${SOURCES})
target_link_libraries(chessvariants SDL2 SDL_image SDL_ttf SDL_mixer ${CONAN_LIBS})
target_include_directories(chessvariants PRIVATE src)
if(WIN32)
  message("Windows detected, including files from include/ folder and linking to files in lib/")
  target_include_directories(chessvariants PRIVATE include)

  target_link_options(chessvariants PRIVATE "-L${CMAKE_CURRENT_LIST_DIR}/lib")
endif()

if(MSVC)
  message("Using MSVC toolchain command line")
  target_compile_options(chessvariants PRIVATE /W4)
else()
  message("Using GCC/Clang toolchain command line")
  target_compile_options(chessvariants PRIVATE -Wall -Wextra -pedantic -Wno-language-extension-token -Werror)
endif()
