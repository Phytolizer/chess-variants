/**
 * @file pimg.hh
 * @author Kyle Coffey (kylecoffey1999@gmail.com)
 * @brief Contains functions related to the custom PIMG file format
 * @date 2020-07-01
 */

#ifndef IO_PIMG_HH
#define IO_PIMG_HH

#include <cstddef>
#include <fstream>
#include <sdl_wrapper/surface/surface.hh>
#include <string>
#include <string_view>


namespace io
{
namespace _internal
{
struct BmpHeader
{
    Uint32 fileSize;
    Uint32 dataOffset;
};
struct DibHeader
{
    Uint16 width;
    Uint16 height;
    Uint16 bpp;
};
struct BmpHeaders
{
    BmpHeader bmp;
    DibHeader dib;
};
BmpHeaders parseBmpHeaders(std::ifstream &reader);
Uint32 readUint32LE(const std::byte *data);
} // namespace _internal
sdl::surface::Surface loadPimg(std::string_view fileName);
} // namespace io

#endif // IO_PIMG_HH