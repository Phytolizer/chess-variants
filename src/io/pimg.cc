#include "pimg.hh"

#include <array>

namespace io
{
namespace _internal
{
BmpHeaders parseBmpHeaders(std::ifstream &reader)
{
    constexpr int BMP_HEADER_SIZE = 14;
    std::array<std::byte, BMP_HEADER_SIZE> bmpHeaderSpace{};
    reader.seekg(0, std::ios::beg);
    reader.read(static_cast<char *>(static_cast<void *>(bmpHeaderSpace.data())), BMP_HEADER_SIZE);
    if (reader.eof())
    {
        throw std::runtime_error("file too small");
    }
    if (!reader.good())
    {
        throw std::runtime_error("failed to read file");
    }
    // validate BMP header
    // starts with ASCII 'BM'
    constexpr int FILE_SZ_OFF = 2;
    constexpr int DATA_OFF_OFF = 10;
    BmpHeader bmpHead{};
    if (bmpHeaderSpace[0] != std::byte('B') || bmpHeaderSpace[1] != std::byte('M'))
    {
        throw std::runtime_error("missing BMP signature at start of file");
    }
    bmpHead.fileSize = readUint32LE(&bmpHeaderSpace[FILE_SZ_OFF]);
    // skip 4 bytes, they are reserved but unused
    bmpHead.dataOffset = readUint32LE(&bmpHeaderSpace[DATA_OFF_OFF]);
    if (bmpHead.dataOffset >= bmpHead.fileSize)
    {
        throw std::runtime_error("BMP data offset is larger than the file size; bad format");
    }

    DibHeader dibHead{};
    return {bmpHead, dibHead};
}
Uint32 readUint32LE(const std::byte *data)
{
    constexpr int BIT1_OFF = 0;
    constexpr int BIT2_OFF = 8;
    constexpr int BIT3_OFF = 16;
    constexpr int BIT4_OFF = 24;
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    Uint32 result = static_cast<Uint32>(data[0] << BIT1_OFF) + static_cast<Uint32>(data[1] << BIT2_OFF) +
                    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
                    static_cast<Uint32>(data[2] << BIT3_OFF) + static_cast<Uint32>(data[3] << BIT4_OFF);
    return result;
}
} // namespace _internal
sdl::surface::Surface loadPimg(std::string_view fileName)
{
    std::ifstream reader(fileName.data(), std::ios::in | std::ios::binary);
    _internal::BmpHeaders headers = _internal::parseBmpHeaders(reader);
    sdl::surface::Surface pimg(headers.dib.width, headers.dib.height, headers.dib.bpp, SDL_PIXELFORMAT_RGBA8888);
    // TODO fill pimg
    return pimg;
}
} // namespace io