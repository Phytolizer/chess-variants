#include "texture.hh"
#include <sdl_wrapper/render/weak_texture.hh>
#include <sdl_wrapper/sdl_exception.hh>
#include <sdl_wrapper/surface/surface.hh>

namespace sdl::render
{

Texture::Texture(WeakRenderer &renderer, Uint32 pixelFormat, int access, int w, int h)
    : WeakTexture(SDL_CreateTexture(renderer.getHandle(), pixelFormat, access, w, h), w, h)
{
    if (getHandle() == nullptr)
    {
        throw SDLException("creating texture from renderer");
    }
}

Texture::Texture(WeakRenderer &renderer, surface::Surface &surface)
    : WeakTexture(SDL_CreateTextureFromSurface(renderer.getHandle(), surface.getHandle()), surface.getHandle()->w,
                  surface.getHandle()->h)
{
    if (getHandle() == nullptr)
    {
        throw SDLException("creating texture from surface");
    }
}

Texture::~Texture()
{
    if (getHandle() != nullptr)
    {
        SDL_DestroyTexture(getHandle());
    }
}

Texture::Texture(Texture &&other) noexcept : WeakTexture(other.getHandle(), other.getWidth(), other.getHeight())
{
    other.setHandle(nullptr);
}

Texture &Texture::operator=(Texture &&other) noexcept
{
    if (&other != this)
    {
        setHandle(other.getHandle());
        setWidth(other.getWidth());
        setHeight(other.getHeight());
        other.setHandle(nullptr);
    }
    return *this;
}
} // namespace sdl::render